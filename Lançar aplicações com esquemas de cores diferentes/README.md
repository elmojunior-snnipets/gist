# Lançar aplicações com esquemas de cores diferentes

Estes procedimentos orientam como executar uma aplicação em um ambiente com **KDE Plasma 5**, utilizando esquemas de cores diferentes do padrão definido nas Configurações.

## Procedimentos

Siga os procedimentos abaixo para configurar e executar os aplicativos desejados.

### 1. Crie uma pasta de configuração alternativa

Para definir qual o esquema de cores será utilizado, é necessário criar uma nova pasta, que será utilizada como alternativa de configuração, para a execução do aplicativo desejado.

É recomendável criar uma pasta oculta dentro do diretório do usuário. Caso prefira, basta utilizar o comando:

```bash
mkdir ~/.config_breeze_escuro
```

### 2. Selecione o esquema de cores desejado

Para selecionar o esquema de cores e salvar dentro do diretório criado no passo anterior, você terá de abrir as **Configurações**, mas informando que deseja salvar os dados dentro da pasta de configuração alternativa.

Para isso, você deverá executar o seguinte comando no terminal:

```bash
export XDG_CONFIG_HOME=~/.config_breeze_escuro/ ; systemsettings5
```

Agora irá abrir o painel de **Configurações**. Vá em "_Cores_", depois selecione o esquema de cores desejado, como _Breeze Escuro_ por exemplo. Depois clique em ''_Aplicar_" e pode fechar a janela de **Configurações**.

### 3. Crie um link simbólico da aplicação desejada

Antes de executar a aplicação desejada, é aconselhável criar um link simbólico dentro da pasta de configuração alternativa, para que não seja necessário configurar toda a aplicação novamente.

Para isso, você deverá executar o comando abaixo no terminal, onde "_aplicação_" se refere ao nome da pasta de configuração do aplicativo:

```bash
ln -s ~/.config/aplicação ~/.config_breeze_escuro/aplicação
```

### 4. Execute o aplicativo

Enfim podemos executar o aplicativo conforme nossas configurações. No terminal você poderá utilizar o seguinte comando, onde "_aplicação_" se refere ao comando para executar o aplicativo:

```bash
export XDG_CONFIG_HOME=~/.config_breeze_escuro/ ; aplicação
```

## Homologação

Este procedimento foi realizado e testado no **KDE Plasma 5.15.5**.

## Referências

- [https://forum.kde.org/viewtopic.php?t=136316](https://forum.kde.org/viewtopic.php?t=136316)
