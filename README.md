# Snippets

Criei este projeto para facilitar o compartilhamento de códigos fonte. Na verdade se trata de um conjunto de trechos de códigos, com diversas finalidades e linguagens diferentes

## Organização

Em cada diretório do projeto é abordado um assunto diferente, podendo conter ou não subdiretórios de acordo com sua necessidade. Os assuntos estão descritos nos nomes de cada diretório.

Dentro de cada diretório poderá conter um arquivo de Texto ou Markdown, com as explicações sobre o assunto abordado. Caso não exista, dentro dos demais arquivos podem haver comentários com as informações sobre o código ou procedimentos que devem ser ralizados.
